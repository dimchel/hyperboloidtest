package com.dimchel.test.hyperboloidtest.classes;

import java.util.ArrayList;

import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by dimchel on 08.09.2015.
 */
public class TaskData
{
    public String id;
    public String title;
    public long date;
    public String text;
    public String longText;
    public String locationText;
    public ArrayList<PriceData> prices;
    public GeoPoint location;
}
