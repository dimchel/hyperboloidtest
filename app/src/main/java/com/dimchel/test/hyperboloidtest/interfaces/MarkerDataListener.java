package com.dimchel.test.hyperboloidtest.interfaces;

import com.dimchel.test.hyperboloidtest.classes.TaskData;

import java.util.ArrayList;

/**
 * Created by dimchel on 08.09.2015.
 */
public interface MarkerDataListener
{
    public void onGetMarkerDataFinished(ArrayList<TaskData> tasks);
    public void onGetMarkerDataFailed();
}
