package com.dimchel.test.hyperboloidtest.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.dimchel.test.hyperboloidtest.classes.TaskData;
import com.dimchel.test.hyperboloidtest.interfaces.MarkerDataListener;
import com.dimchel.test.hyperboloidtest.utilty.Fetcher;
import com.dimchel.test.hyperboloidtest.utilty.JsonMethods;

import java.util.ArrayList;

/**
 * Created by dimchel on 08.09.2015.
 */
public class GetMarkerDataRequestTask extends AsyncTask<String, Void, String>
{
    private MarkerDataListener delegate;

    public GetMarkerDataRequestTask(MarkerDataListener delegate)
    {
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... params)
    {
        Log.v("gg", "url: " + params[0]);

        // trying to send request
        return Fetcher.getUrl(params[0]);
    }

    @Override
    protected void onPostExecute(String response)
    {
        super.onPostExecute(response);

        if (response != null)
        {
            ArrayList<TaskData> data = new ArrayList<TaskData>();
            data = JsonMethods.parseMarkersData(response);
            Log.v("gg", "response:" + response);
            delegate.onGetMarkerDataFinished(data);
        }
        else
            delegate.onGetMarkerDataFailed();
    }
}
