package com.dimchel.test.hyperboloidtest.data;

import com.dimchel.test.hyperboloidtest.classes.TaskData;

import java.util.ArrayList;

/**
 * Created by dimchel on 07.09.2015.
 */
public class AppDataManager
{
    // app data
    public static ArrayList<TaskData> tasks;

    // app methods
    public static TaskData findTaskById(String id)
    {
        if (tasks == null)
            return null;

        if (tasks.isEmpty())
            return null;

        for (int i = 0; i < tasks.size(); i++)
        {
            if (tasks.get(i).id.equals(id))
                return tasks.get(i);
        }

        return null;
    }
}
