package com.dimchel.test.hyperboloidtest.utilty;

import com.dimchel.test.hyperboloidtest.classes.PriceData;
import com.dimchel.test.hyperboloidtest.classes.TaskData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import ru.yandex.yandexmapkit.utils.GeoPoint;

/**
 * Created by dimchel on 08.09.2015.
 */
public class JsonMethods
{
    public static ArrayList<TaskData> parseMarkersData(String response)
    {
        ArrayList<TaskData> result = new ArrayList<TaskData>();
        try
        {
            JSONObject dataJ = new JSONObject(response);
            JSONArray tasksJ = dataJ.getJSONArray("tasks");

            for (int i = 0; i < tasksJ.length(); i++)
            {
                TaskData task = new TaskData();
                task.id = tasksJ.getJSONObject(i).getString("ID");
                task.title = tasksJ.getJSONObject(i).getString("title");
                task.date = tasksJ.getJSONObject(i).getLong("date");
                task.text = tasksJ.getJSONObject(i).getString("text");
                task.longText = tasksJ.getJSONObject(i).getString("longText");
                task.locationText = tasksJ.getJSONObject(i).getString("locationText");

                task.prices = new ArrayList<PriceData>();
                JSONArray pricesJ = tasksJ.getJSONObject(i).getJSONArray("prices");
                for (int j = 0; j < pricesJ.length(); j++)
                {
                    PriceData price = new PriceData();
                    price.price = pricesJ.getJSONObject(j).getInt("price");
                    price.description = pricesJ.getJSONObject(j).getString("description");

                    task.prices.add(price);
                }

                GeoPoint location = new GeoPoint();
                location.setLat(tasksJ.getJSONObject(i).getJSONObject("location").getInt("lat"));
                location.setLon(tasksJ.getJSONObject(i).getJSONObject("location").getInt("lon"));
                task.location = location;

                result.add(task);
            }

            return result;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
