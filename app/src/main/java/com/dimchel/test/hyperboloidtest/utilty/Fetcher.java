package com.dimchel.test.hyperboloidtest.utilty;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by dimchel on 08.09.2015.
 */
public class Fetcher
{
    public static String getUrl(String urlSpec)
    {
        try
        {
            return new String(getUrlBytes(urlSpec));
        }
        catch (Exception e)
        {
            return null;
        }
    }

    public static byte[] getUrlBytes(String urlSpec) throws Exception
    {

        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Accept-Language", "ru-RU,ru");
        connection.setRequestMethod("GET");

        try
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
            {
                return null;
            }

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        }
        catch (IOException e)
        {
            return null;
        }
    }
}