package com.dimchel.test.hyperboloidtest.data;

import com.dimchel.test.hyperboloidtest.interfaces.MarkerDataListener;
import com.dimchel.test.hyperboloidtest.tasks.GetMarkerDataRequestTask;

/**
 * Created by dimchel on 07.09.2015.
 */
public class WebService
{
    // web urls
    private String baseUrl = "http://test.boloid.com:9000/tasks";

    private static WebService instanceService = new WebService();

    public static WebService getInstanceService()
    {
        return instanceService;
    }

    // web methods
    public void onGetMarketData(MarkerDataListener delegate)
    {
        GetMarkerDataRequestTask gmdrt = new GetMarkerDataRequestTask(delegate);
        gmdrt.execute(baseUrl);
    }
}
