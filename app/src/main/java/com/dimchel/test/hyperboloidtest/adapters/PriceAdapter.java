package com.dimchel.test.hyperboloidtest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.dimchel.test.hyperboloidtest.R;
import com.dimchel.test.hyperboloidtest.classes.PriceData;

import java.util.ArrayList;

/**
 * Created by dimchel on 08.09.2015.
 */
public class PriceAdapter extends ArrayAdapter<PriceData>
{
    private Context context;
    private ArrayList<PriceData> priceArray;

    public PriceAdapter(Context context, ArrayList<PriceData> priceArray)
    {
        super(context, R.layout.item_price, priceArray);

        this.context = context;
        this.priceArray = priceArray;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder = null;

        if (convertView == null)
        {
            // creating view
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_price, parent, false);
            holder = new ViewHolder();

            holder.editPrice = (EditText) convertView.findViewById(R.id.editPrice);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtName);

            // trying to fill fields
            try
            {
                holder.txtName.setText(priceArray.get(position).description);
                holder.editPrice.setText(String.valueOf(priceArray.get(position).price));
            }
            catch (Exception e) {}
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }

    static class ViewHolder
    {
        EditText editPrice;
        TextView txtName;
    }
}
