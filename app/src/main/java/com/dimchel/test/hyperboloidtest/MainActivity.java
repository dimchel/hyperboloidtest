package com.dimchel.test.hyperboloidtest;

import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dimchel.test.hyperboloidtest.classes.TaskData;
import com.dimchel.test.hyperboloidtest.data.AppDataManager;
import com.dimchel.test.hyperboloidtest.data.WebService;
import com.dimchel.test.hyperboloidtest.interfaces.MarkerDataListener;

import java.util.ArrayList;
import java.util.List;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.overlay.balloon.BalloonItem;
import ru.yandex.yandexmapkit.overlay.balloon.OnBalloonListener;
import ru.yandex.yandexmapkit.utils.GeoPoint;

public class MainActivity extends AppCompatActivity implements MarkerDataListener
{
    private Menu optionsMenu;

    private MapController mapController;
    private OverlayManager overlayManager;
    private Overlay overlay;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        // init yandex map
        MapView mapView = (MapView) findViewById(R.id.map);

        mapController = mapView.getMapController();
        overlayManager = mapController.getOverlayManager();
        // disable finding location
        overlayManager.getMyLocation().setEnabled(false);

        // set refreshing progress bar
        setRefreshActionButtonState(true);
        // request to the server
        WebService.getInstanceService().onGetMarketData(this);
    }

    private void refreshMap(final ArrayList<TaskData> tasks)
    {
        if (tasks != null)
        {
            // new overlay
            overlay = new Overlay(mapController);

            for (int i = 0; i < tasks.size(); i++)
            {
                // adding new marker
                OverlayItem task = new OverlayItem(tasks.get(i).location,
                        ResourcesCompat.getDrawable(getResources(), R.drawable.marker_icon, null));

                // init infoWindow with text
                BalloonItem balloonKremlin = new BalloonItem(this, task.getGeoPoint());
                balloonKremlin.setText(tasks.get(i).title);

                final String id = tasks.get(i).id;
                balloonKremlin.setOnBalloonListener(new OnBalloonListener()
                {
                    @Override
                    public void onBalloonViewClick(BalloonItem balloonItem, View view)
                    {
                        // pass task id to the TaskActivity
                        Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                        intent.putExtra("task_id", id);
                        startActivity(intent);
                    }

                    @Override
                    public void onBalloonShow(BalloonItem balloonItem) {}

                    @Override
                    public void onBalloonHide(BalloonItem balloonItem) {}

                    @Override
                    public void onBalloonAnimationStart(BalloonItem balloonItem) {}

                    @Override
                    public void onBalloonAnimationEnd(BalloonItem balloonItem) {}
                });

                task.setBalloonItem(balloonKremlin);
                overlay.addOverlayItem(task);
            }

            overlayManager.addOverlay(overlay);
            // set normal zoom
            setZoomSpan(tasks.size());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.airport_menuRefresh:
            {
                // send GET request to the server
                setRefreshActionButtonState(true);
                WebService.getInstanceService().onGetMarketData(this);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    //
    private void setRefreshActionButtonState(boolean refreshing)
    {
        if (optionsMenu != null)
        {
            final MenuItem refreshItem = optionsMenu.findItem(R.id.airport_menuRefresh);
            if (refreshItem != null)
            {
                if (refreshing)
                    refreshItem.setActionView(R.layout.action_view_main);
                else
                    refreshItem.setActionView(null);
            }
        }
    }

    @Override
    public void onGetMarkerDataFinished(ArrayList<TaskData> tasks)
    {
        setRefreshActionButtonState(false);

        AppDataManager.tasks = tasks;

        refreshMap(tasks);
    }

    @Override
    public void onGetMarkerDataFailed()
    {
        setRefreshActionButtonState(false);
        Toast.makeText(this, getResources().getString(R.string.mes_no_connection), Toast.LENGTH_SHORT).show();

        refreshMap(AppDataManager.tasks);
    }

    private void setZoomSpan(int count)
    {
        List<OverlayItem> list = overlay.getOverlayItems();
        double maxLat, minLat, maxLon, minLon;
        maxLat = maxLon = Double.MIN_VALUE;
        minLat = minLon = Double.MAX_VALUE;
        for (int i = 0; i < count; i++)
        {
            GeoPoint geoPoint = list.get(i).getGeoPoint();
            double lat = geoPoint.getLat();
            double lon = geoPoint.getLon();

            maxLat = Math.max(lat, maxLat);
            minLat = Math.min(lat, minLat);
            maxLon = Math.max(lon, maxLon);
            minLon = Math.min(lon, minLon);
        }
        mapController.setZoomToSpan(maxLat - minLat, maxLon - minLon);
        mapController.setPositionAnimationTo(new GeoPoint((maxLat + minLat) / 2, (maxLon + minLon) / 2));
    }
}
