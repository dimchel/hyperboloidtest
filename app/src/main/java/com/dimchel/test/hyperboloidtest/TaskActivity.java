package com.dimchel.test.hyperboloidtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dimchel.test.hyperboloidtest.adapters.PriceAdapter;
import com.dimchel.test.hyperboloidtest.classes.TaskData;
import com.dimchel.test.hyperboloidtest.data.AppDataManager;

public class TaskActivity extends AppCompatActivity
{
    private TaskData currentTask;
    private ListView lvPrices;
    private EditText editTitle, editDescription, editDate, editLocation;
    private TextView txtPrices;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        // load extras
        Bundle extras = getIntent().getExtras();
        if (extras != null)
        {
            String id = extras.getString("task_id");
            currentTask = AppDataManager.findTaskById(id);
        }

        // adding back-button to the action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (currentTask != null)
        {
            // set title to the action bar
            getSupportActionBar().setTitle(currentTask.title);

            // init listview with header and footer
            lvPrices = (ListView) findViewById(R.id.lvPrices);


            View header = getLayoutInflater().inflate(R.layout.task_header, null);

            editTitle = (EditText) header.findViewById(R.id.editTitle);
            editDescription = (EditText) header.findViewById(R.id.editDescription);
            txtPrices = (TextView) header.findViewById(R.id.txtPrices);

            editTitle.setText(currentTask.text);
            editDescription.setText(currentTask.longText);

            if (currentTask.prices.isEmpty())
                txtPrices.setVisibility(View.GONE);

            lvPrices.addHeaderView(header);


            View footer = getLayoutInflater().inflate(R.layout.task_footer, null);

            editDate = (EditText) footer.findViewById(R.id.editDate);
            editLocation = (EditText) footer.findViewById(R.id.editLocation);

            editDate.setText(String.valueOf(currentTask.date));
            editLocation.setText(currentTask.locationText);

            lvPrices.addFooterView(footer);

            PriceAdapter priceAdapter = new PriceAdapter(this, currentTask.prices);
            lvPrices.setAdapter(priceAdapter);
            lvPrices.setDividerHeight(2);
        }
        else
            Toast.makeText(this, getResources().getString(R.string.mes_error), Toast.LENGTH_SHORT).show();
    }
}
